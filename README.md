Buenos días,

La practica para recuperar los 20 puntos de la unidad 2 consiste en crear los siguientes queries utilizando las funciones lambda de Entity Framework. 

Sigan los siguientes pasos para terminarla.
Crear un fork del siguiente repositorio que contiene los modelos para hacer la practica https://bitbucket.org/carlosomarblanco/lamdapractice (Como crear forks)
Clonar su fork a su computadora.
Corregir las relaciones entre los modelos.
Crear los siguientes queries utilizando lambdas.
Listar todos los empleados cuyo departamento tenga una sede en Chihuahua
Listar todos los departamentos y el numero de empleados que pertenezcan a cada departamento.
Listar todos los empleados remotos. Estos son los empleados cuya ciudad no se encuentre entre las sedes de su departamento.
Listar todos los empleados cuyo aniversario de contratación sea el próximo mes.
Listar los 12 meses del año y el numero de empleados contratados por cada mes.
Hacer push de los cambios a su fork del repositorio y darme acceso a el para revisarlo.

Cada query tiene un valor de 4 pts. La hora de entrega es este Domingo 12 de Abril antes de las 2 pm. Cualquier duda por este medio.


PD. El equipo que reviso la aplicación el lunes 6 de Abril no es necesario que lo entregue, puede usar el material como referencia.