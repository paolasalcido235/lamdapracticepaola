﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice.Data
{
    public class Department
    {
          public Department()
        {
            this.Employees = new HashSet<Employee>();
            this.Cities = new HashSet<City>();
          
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

         public virtual ICollection<Employee> Employees { get; set; }
         public virtual ICollection<City> Cities { get; set; }
    }
}
