﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice.Data
{
    public class DatabaseContext : DbContext
    {

        public DatabaseContext()
            : base("EmployeesDB") 
            {
               //Database.SetInitializer<DatabaseContext>(new DatabaseContextInitializer<DatabaseContext>());
                //Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, LamdaPractice.Migrations.Configuration>("EmployeesDB"));
            }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Department> Departments { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
      
            base.OnModelCreating(modelBuilder);
        }
    }
}
