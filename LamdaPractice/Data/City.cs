﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice.Data
{
    public class City
    {
         public City()
        {

            this.Employees= new HashSet<Employee>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
      
        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
