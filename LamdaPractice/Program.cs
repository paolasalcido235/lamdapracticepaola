﻿using LamdaPractice.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamdaPractice
{
    class Program
    {
       static DatabaseContext db = new DatabaseContext();
      
        static void Main(string[] args)
        {

         
            using (var ctx = new DatabaseContext())
            {
                ctx.Cities.ToList().ForEach(c => Console.WriteLine(c.Name));
                empleadosChihuahua();
                empleadosPorDepartamento();
                empleadosContratadosSigMes();
                empleadosContratadosPorMes();
                empleadosRemoto();
            }

            Console.Read();
        }

        private static void empleadosContratadosSigMes()
        {
        
            DateTime nextMonth = DateTime.Now.AddYears(-1);
           DateTime nextMonth2 = DateTime.Now.AddMonths(1).AddYears(-1);


            Console.WriteLine("Listar Empleados cuyo aniversario sea desde: "+ nextMonth.Date
                + " Hasta " + nextMonth2.Date);
         
              db.Employees.ToList().
                    FindAll(c => (c.HireDate.Date > nextMonth.Date) && (c.HireDate.Date < nextMonth2.Date)).
                        ForEach(e => Console.WriteLine(e.
                            FirstName + "  " + e.LastName + "  " + e.HireDate)); 



        }


        private static void empleadosContratadosPorMes()
        {
            Console.WriteLine("Listar Empleados por mes");

            var groupedCategories = db.Employees.GroupBy(m => m.HireDate.Month).ToList().
                Select(c => new { ID = c.Key, Count = c.Count() }).ToList();
            var list = groupedCategories.AsEnumerable().OrderBy(e => e.ID).ToList();
            list.ForEach(e => Console.WriteLine("Mes : " + e.ID +"  Contratados: " +e.Count));
           /* var groupedCategories = db.Employees.GroupBy(m => m.HireDate.Month);
         
            groupedCategories.ToList().
              ForEach(c => Console.WriteLine(c.Count()+ "  "  ));*/
           
        }

        private static void empleadosRemoto()
        {
            Console.WriteLine("Empleados que no estan en chihuahua");
            var cdid = db.Cities.Where(e=> e.Name != "Chihuahua").ToList();
           
            foreach (var ciudad in cdid)
            {
                

               db.Employees.ToList().
                 FindAll(c => c.City.Id == ciudad.Id).
               ForEach(e => Console.WriteLine(e.City.Name+ "  " + e.FirstName + "  " + e.LastName)); 
            }



        }

        private static void empleadosChihuahua()
        {
            Console.WriteLine("Empleados en chihuahua");

           
            var cdid = db.Cities.Where(e => e.Name == "Chihuahua").ToList();

            foreach (var ciudad in cdid)
            {


                db.Employees.ToList().
                  FindAll(c => c.City.Id == ciudad.Id).
                ForEach(e => Console.WriteLine(e.City.Name + "  " + e.FirstName + "  " + e.LastName));
            }



            
            /* db.Employees.ToList().
           FindAll(c => c.City.Id == (db.Cities.
               First(dpt => dpt.Name == "Chihuahua").Id)).
               ForEach(e => Console.WriteLine(e.FirstName +"  "+ e.LastName)); */

        }

        public static void empleadosPorDepartamento()
        {
            Console.WriteLine("Empleados por departamento");
           
            db.Departments.ToList().
                ForEach(dpt => Console.WriteLine(db.Employees.ToList().
                    Where(e => e.Department.Id == dpt.Id).Count() + " " + dpt.Name));
         }

    }
}
